package com.virtualpairprogrammers.recommender.systems;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.ml.recommendation.ALS;
import org.apache.spark.ml.recommendation.ALSModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.log;

public class VPPCourseRecommendations {
    static void logInfo(Object o) { Logger.getLogger("com.virtualpairprogrammers").info("***   -->   " +o);}
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkSession spark = SparkSession.builder()
                .appName("Gym Competitors")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .master("local[*]").getOrCreate();
        Dataset<Row> csvData = spark.read()
                .option("header", true)
                .option("inferSchema", true)
                .csv("src/main/resources/VPPcourseViews.csv");
        /** csvData.show();
         * +------+--------+-----------------+
         * |userId|courseId|proportionWatched|
         * +------+--------+-----------------+
         * |     1|       1|             0.15|
         * |     3|       1|             0.64|
         * |     4|       1|              1.0|
         * */
        csvData = csvData
                .withColumn("proportionWatched", col("proportionWatched").multiply(100))
                .withColumnRenamed("proportionWatched", "percentWatched");
        /** csvData.show();
         * |userId|courseId|percentWatched|
         * +------+--------+--------------+
         * |     1|       1|          15.0|
         * */
        /** csvData.groupBy("userId").pivot("courseId").sum("percentWatched").show();
         * +------+------+------+-----+-----+-----+-----+------+-----+-----+-----+-----+-----+-----+-----+-----+------+-----+-----+-----+-----+
         * |userId|     1|     2|    3|    4|    5|    6|     7|    8|    9|   10|   11|   12|   13|   14|   15|    16|   17|   18|   19|   20|
         * +------+------+------+-----+-----+-----+-----+------+-----+-----+-----+-----+-----+-----+-----+-----+------+-----+-----+-----+-----+
         * |   496|  null|  62.0| 51.0| 35.0|  9.0| 26.0|  null| 39.0| 53.0| 94.0|100.0| 97.0| 83.0| 78.0| 7.00|  23.0| null| null|100.0| 98.0|
         * |   471| 28.00|  74.0| 63.0| 63.0| 48.0| 19.0|  33.0| 19.0| null| null| null| 22.0| 17.0| null| 66.0|  97.0| 59.0| 18.0| null| 91.0|
         * |   463|  null| 14.00| 68.0| null|100.0|  1.0|  50.0| 27.0| null| null| null| 50.0|  8.0| null| 67.0| 55.00| 33.0| 92.0| null| 64.0|
         * |   148|  null|  null| null|14.00| null| null| 14.00| null| null| null| null| null| null| null| null|  null|100.0|100.0| null| 20.0|
         * */

        //No need to split data in unsupervised learning
//        Dataset<Row>[] splitCsvData = csvData.randomSplit(new double[]{0.9, 0.1});
//        Dataset<Row> trainingData = splitCsvData[0];
//        Dataset<Row> holdoutData = splitCsvData[1];

        ALS als = new ALS() .setMaxIter(10)
                            .setRegParam(0.1)
                            .setUserCol("userId")
                            .setItemCol("courseId")
                            .setRatingCol("percentWatched");

//        ALSModel alsModel = als.fit(trainingData);
//        Dataset<Row> predictions = alsModel.transform(holdoutData);
//        predictions.show();

        ALSModel alsModel = als.fit(csvData);
        alsModel.setColdStartStrategy("drop");  // default "NaN", drop-strategy removes these records

//        Dataset<Row> userRecommendations = alsModel.recommendForAllUsers(5);
//        /** userRecommendations.show();
//         * +------+--------------------+
//         * |userId|     recommendations|
//         * +------+--------------------+
//         * |   471|[[16, 94.45463], ...|
//         * |   463|[[1, 160.19336], ...|
//         * |   496|[[19, 101.97571],...|
//         * |   148|[[17, 99.9746], [...|
//         * |   540|[[13, 105.37329],...|*/
//        List<Row> userRecommendationsList = userRecommendations.takeAsList(5);
//        /** userRecommendationsList.forEach(VPPCourseRecommendations::logInfo);
//         * [471,   WrappedArray([16,94.45463],   [20,88.72645],   [11,69.42189],   [2,66.61108],   [19,66.057686])]
//         * [463,   WrappedArray([1,160.19336],   [5,98.54848],    [18,92.128235],  [3,68.1441],    [15,65.80455])]
//         * [496,   WrappedArray([19,101.97571],  [10,94.53485],   [20,92.13925],   [12,90.02681],  [14,84.539604])]
//         * [148,   WrappedArray([17,99.9746],    [18,99.8747],    [9,60.172997],   [1,42.248226],  [11,39.141933])]
//         * [540,   WrappedArray([13,105.37329],  [19,100.691765], [14,100.00784],  [10,95.34011],  [12,91.614914])]
//         * */
//        for (Row r : userRecommendationsList) {
//            int userId = r.getAs(0);
//            String recs = r.getAs(1).toString();
//            logInfo("User " + userId + " might recommend: " + recs);    // some recommends have alrdy been watched
//            logInfo("USER HAS ALRDY WATCHED :");
//            csvData.filter("userId = "+userId).show();
//        }
        Dataset<Row> myCsvDataset = spark.read()
                .option("header", true)
                .option("inferSchema", true)
                .csv("src/main/resources/myCsv.csv");
        alsModel.transform(myCsvDataset).show();
        alsModel.recommendForUserSubset(myCsvDataset, 5).show();


        spark.close();
    }
}
