package com.virtualpairprogrammers.logistic.regression;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
import org.apache.spark.ml.classification.LogisticRegressionSummary;
import org.apache.spark.ml.evaluation.RegressionEvaluator;
import org.apache.spark.ml.feature.OneHotEncoderEstimator;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.ml.tuning.TrainValidationSplit;
import org.apache.spark.ml.tuning.TrainValidationSplitModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class VPPChapterViewLogisticReg {
    static void logInfo(Object o) { Logger.getLogger("com.virtualpairprogrammers").info("   *** MY_LOG ***   -->   " +o);}
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkSession spark = SparkSession.builder()
                .appName("VPP Chapter Views")
                .config("spark.sql.warehouse.dir","file:///c:/tmp/")
                .master("local[*]").getOrCreate();
        Dataset<Row> csvData = spark.read()
                .option("header", true)
                .option("inferSchema", true)
                .csv("src/main/resources/vppChapterViews/*.csv");

        /**+-------------------+-------+-----------------------+--------+-------------------+---+--------------+----------------+----------------+------------+
         * |payment_method_type|country|rebill_period_in_months|firstSub|   observation_date|age|all_time_views|last_month_views|next_month_views|is_cancelled|
         * +-------------------+-------+-----------------------+--------+-------------------+---+--------------+----------------+----------------+------------+
         * |            SAGEPAY|    gbr|                      1|     1.0|2018-03-01 00:00:00|887|           170|            null|            null|        true|
         * |             PAYPAL|    usa|                      1|     1.0|2018-03-01 00:00:00|787|             7|            null|            null|        true|
         * |            SAGEPAY|    aus|                      1|     1.0|2018-03-01 00:00:00|786|           521|            null|            null|        true|
         **/
        csvData = csvData.filter("is_cancelled = false").drop("observation_date","is_cancelled");

        csvData = csvData
                .withColumn("firstSub",
                        when( col("firstSub").isNull(), 0 ).otherwise(col("firstSub")))
                .withColumn("all_time_views",
                        when (col("all_time_views").isNull(),0).otherwise(col("all_time_views")))
                .withColumn("last_month_views",
                        when (col("last_month_views").isNull(),0).otherwise(col("last_month_views")))
                .withColumn("next_month_views",
                        //  label: customers_watched_no_videos (0 = false, 1 = true)
                        when (col("next_month_views").$greater(0),0).otherwise(1));
        csvData = csvData.withColumnRenamed("next_month_views", "label");

        StringIndexer payMethodIndexer = new StringIndexer();
        csvData = payMethodIndexer.setInputCol("payment_method_type")
                .setOutputCol("payIndex")
                .fit(csvData)
                .transform(csvData);
        StringIndexer countryIndexer = new StringIndexer();
        csvData = countryIndexer.setInputCol("country")
                .setOutputCol("countryIndex")
                .fit(csvData)
                .transform(csvData);
        StringIndexer periodIndexer = new StringIndexer();
        csvData = periodIndexer.setInputCol("rebill_period_in_months")
                .setOutputCol("periodIndex")
                .fit(csvData)
                .transform(csvData);

        OneHotEncoderEstimator encoder = new OneHotEncoderEstimator();
        csvData = encoder.setInputCols(new String[] {"payIndex","countryIndex","periodIndex"})
                .setOutputCols(new String[]{"payVector","countryVector","periodVector"})
                .fit(csvData).transform(csvData);

        VectorAssembler vectorAssembler = new VectorAssembler();
        Dataset<Row> inputData = vectorAssembler.setInputCols(new String[] {"firstSub","age","all_time_views","last_month_views",
                "payVector","countryVector","periodVector"})
                .setOutputCol("features")
                .transform(csvData).select("label","features");

        Dataset<Row>[] trainAndHoldoutData = inputData.randomSplit(new double[] {0.9,0.1});
        Dataset<Row> trainAndTestData = trainAndHoldoutData[0];
        Dataset<Row> holdOutData = trainAndHoldoutData[1];

        LogisticRegression logisticRegression = new LogisticRegression();
        ParamGridBuilder paramGridBuilder = new ParamGridBuilder();
        ParamMap[] paramMap = paramGridBuilder
                .addGrid(logisticRegression.regParam(), new double[] {0.01,0.1,0.3,0.5,0.7,1})
                .addGrid( logisticRegression.elasticNetParam(),new double[] {0,0.5,1}).build();

        TrainValidationSplit tvs = new TrainValidationSplit();
        tvs.setEstimator(logisticRegression)
                .setEvaluator(new RegressionEvaluator().setMetricName("r2"))
                .setEstimatorParamMaps(paramMap)
                .setTrainRatio(0.9);

        TrainValidationSplitModel model = tvs.fit(trainAndTestData);

        LogisticRegressionModel lrModel = (LogisticRegressionModel)model.bestModel();
        logInfo("Accuracy Score = " + lrModel.summary().accuracy());

        logInfo("coefficients : " + lrModel.coefficients() + " intercept : " + lrModel.intercept());
        logInfo("reg param : " + lrModel.getRegParam() + " elastic net param : " + lrModel.getElasticNetParam());

        LogisticRegressionSummary summary = lrModel.evaluate(holdOutData);
        double truePositives = summary.truePositiveRateByLabel()[1];
        double falsePositives = summary.falsePositiveRateByLabel()[0];

        logInfo("TruePositives : " + truePositives);
        logInfo("falsePositives : " + falsePositives);
        logInfo("accuracy on positive predictions : " + truePositives / (truePositives + falsePositives));
        logInfo("lrModel.evaluate(holdOutData).accuracy() : " + summary.accuracy());

        lrModel.transform(holdOutData).groupBy(col("label"), col("prediction")).count().show();


        spark.close();
    }
}
