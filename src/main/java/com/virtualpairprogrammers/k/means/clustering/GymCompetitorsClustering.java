package com.virtualpairprogrammers.k.means.clustering;

import org.apache.commons.math3.ml.clustering.evaluation.ClusterEvaluator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.evaluation.ClusteringEvaluator;
import org.apache.spark.ml.feature.OneHotEncoderEstimator;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;

public class GymCompetitorsClustering {
static void logInfo(Object o) { Logger.getLogger("com.virtualpairprogrammers").info("***   -->   " +o);}
public static void main(String[] args) {
    System.setProperty("hadoop.home.dir", "c:/hadoop");
    Logger.getLogger("org.apache").setLevel(Level.WARN);
    SparkSession spark = SparkSession.builder()
            .appName("Gym Competitors")
            .config("spark.sql.warehouse.dir","file:///c:/tmp/")
            .master("local[*]").getOrCreate();
    Dataset<Row> csvData = spark.read()
            .option("header", true)
            .option("inferSchema", true)
            .csv("src/main/resources/GymCompetition.csv");

    csvData.show();
    csvData.printSchema();

    StringIndexer genderIndexer = new StringIndexer();
		genderIndexer.setInputCol("Gender");
		genderIndexer.setOutputCol("GenderIndex");
    csvData = genderIndexer.fit(csvData).transform(csvData);

    OneHotEncoderEstimator genderEncoder = new OneHotEncoderEstimator();
        genderEncoder.setInputCols(new String[] {"GenderIndex"});
        genderEncoder.setOutputCols(new String[] {"GenderVector"});
    csvData = genderEncoder.fit(csvData).transform(csvData);
    csvData.show();

    VectorAssembler vectorAssembler = new VectorAssembler();
    Dataset<Row> inputData = vectorAssembler
            .setInputCols(new String[]{"GenderVector", "Age", "Height", "Weight", "NoOfReps"})
            .setOutputCol("features")
            .transform(csvData)
            .select("features");
    inputData.show();

    KMeans kMeans = new KMeans();
    for (int noOfClusters = 2; noOfClusters <= 8; noOfClusters++) {
        kMeans.setK(noOfClusters);
        KMeansModel kMeansModel = kMeans.fit(inputData);
        Dataset<Row> predictions = kMeansModel.transform(inputData);
        /** predictions.show();
         * +--------------------+----------+
         * |            features|prediction|
         * +--------------------+----------+
         * |[1.0,23.0,180.0,8...|         2|
         * |[1.0,18.0,169.0,8...|         2|
         * |[1.0,25.0,174.0,7...|         0|
         * |[0.0,24.0,166.0,6...|         1|
         * */
        Vector[] clusterCenters = kMeansModel.clusterCenters();
        for (int i = 0; i < clusterCenters.length; i++) logInfo("Cluster "+i+ ": " +clusterCenters[i]);
        /** vectorAss.setInputCols(new String[]{
         * "GenderVector",      "Age", "Height", "Weight",          "NoOfReps"})
         * ---------------------------------------------------------------------------------------+
         * [0.3333333333333333, 22.0,   172.0   ,71.33333333333333  ,50.0]                        |  --> cluster0
         * [0.0,                22.5,   167.75  ,60.0               ,44.75]                       |  --> cluster1
         * [0.8,                22.1,   179.0   ,83.60000000000001  ,54.300000000000004]          |  --> cluster2
         * ---------------------------------------------------------------------------------------+
         * 0: male, 1:fem       averages....
         * */

        predictions.groupBy("prediction").count().show();

        logInfo("SSE is " + kMeansModel.computeCost(inputData));

        ClusteringEvaluator evaluator = new ClusteringEvaluator();
        logInfo("Silhouette with squared euclidean distance is " + evaluator.evaluate(predictions));
    }


    spark.close();
    }
}