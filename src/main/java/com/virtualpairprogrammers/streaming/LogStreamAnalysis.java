package com.virtualpairprogrammers.streaming;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple2;

public class LogStreamAnalysis {
    static void logInfo(Object o) { Logger.getLogger("com.virtualpairprogrammers").info("***   -->   "+o);}
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        Logger.getLogger("org.apache.spark.storage").setLevel(Level.ERROR); // warnings because not running in cluster

        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("logStreamsAnal");
        JavaStreamingContext sc = new JavaStreamingContext(conf, Durations.seconds(10));
        JavaReceiverInputDStream<String> inputData = sc.socketTextStream("localhost", 8989);

        JavaPairDStream<String, Long> result = inputData
                .map(s -> s.split(",")[0])
                .mapToPair(s -> new Tuple2<>(s, 1L))
//                .reduceByKey(Long::sum);
                .reduceByKeyAndWindow(Long::sum, Durations.minutes(2L)); // include results from batches in window (duration)
        result.print();

        sc.start();
        sc.awaitTermination();
    }
}
