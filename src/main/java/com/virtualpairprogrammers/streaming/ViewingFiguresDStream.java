package com.virtualpairprogrammers.streaming;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;

import java.util.*;

public class ViewingFiguresDStream {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        Logger.getLogger("org.apache.spark.storage").setLevel(Level.ERROR); //warnings because not running in cluster

        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("ViewingFigures");
        JavaStreamingContext sc = new JavaStreamingContext(conf, Durations.seconds(1L)); // keep small (performance)

        Collection<String> topics = Collections.singletonList("viewrecords");
        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", "localhost:9092"); //spark server
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "spark-group-1");   //required, u can put * consumers in a group -> if 1 member consumes event, all other members consider this event as consumed
        kafkaParams.put("auto.offset.reset", "latest"); //https://medium.com/@danieljameskay/understanding-the-enable-auto-commit-kafka-consumer-property-12fa0ade7b65
        kafkaParams.put("enable.auto.commit", false);

        JavaInputDStream<ConsumerRecord<String, String>> inputDStream = KafkaUtils.createDirectStream(
                sc,                                                                         //javaStreamingContext
                LocationStrategies.PreferConsistent(),                                      //locationStrategy
                ConsumerStrategies.Subscribe(                                               //consumerStrategy
                        topics,                                                                 // Collection of topics
                        kafkaParams                                                             // Map of params
                ));

        inputDStream
                .mapToPair(record -> new Tuple2<>(record.value(), 5L))
                .reduceByKeyAndWindow(Long::sum, Durations.seconds(30L))        // window = multiple of batch_size (see JavaStreamingContext constructor, param2)
                .mapToPair(Tuple2::swap)                                                            // swaps key/value
                .transformToPair(longStringJavaPairRDD -> longStringJavaPairRDD.sortByKey(false))
                .print();


        sc.start();
        sc.awaitTermination();
    }
}
