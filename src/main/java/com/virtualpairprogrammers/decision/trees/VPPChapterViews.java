package com.virtualpairprogrammers.decision.trees;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.ml.classification.DecisionTreeClassificationModel;
import org.apache.spark.ml.classification.DecisionTreeClassifier;
import org.apache.spark.ml.classification.RandomForestClassificationModel;
import org.apache.spark.ml.classification.RandomForestClassifier;
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.feature.IndexToString;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.regression.DecisionTreeRegressionModel;
import org.apache.spark.ml.regression.DecisionTreeRegressor;
import org.apache.spark.ml.regression.Regressor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.DataTypes;

import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.*;

public class VPPChapterViews {
    static void logInfo(Object o) { Logger.getLogger("com.virtualpairprogrammers").info("   *** MY_LOG ***   -->   " +o);}
    public static UDF1<String,String> countryGrouping = country -> {
        List<String> topCountries =  Arrays.asList("GB","US","IN","UNKNOWN");
        List<String> europeanCountries =  Arrays.asList("BE","BG","CZ","DK","DE","EE","IE","EL","ES","FR","HR","IT","CY","LV","LT","LU","HU","MT","NL","AT","PL","PT","RO","SI","SK","FI","SE","CH","IS","NO","LI","EU");
        if (topCountries.contains(country)) return country;
        if (europeanCountries .contains(country)) return "EUROPE";
        else return "OTHER";
    };

    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkSession sparkSession = SparkSession.builder()
                .appName("VPP Chapter Views")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .master("local[*]").getOrCreate();
        sparkSession.udf().register("countryGroupingUDF", countryGrouping, DataTypes.StringType);

        Dataset<Row> dataset = sparkSession.read()
                .option("header", true)
                .option("inferSchema", true)
                .csv("src/main/resources/vppChapterViews/vppFreeTrials.csv");
        /**        dataset.show();
         * +-------+-------------+-------------+--------------------+---------------+
         * |country|rebill_period|payments_made|chapter_access_count|seconds_watched|
         * +-------+-------------+-------------+--------------------+---------------+
         * |UNKNOWN|            1|            1|                   3|           9406|
         * |     US|            6|            1|                   2|         189265|
         * |     US|           12|            2|                   3|         255055|
         * |     US|            1|            0|                   7|           1354|
         * |     GB|            1|            1|                   5|           4543|
         * |     AU|            1|            0|                   8|           2817|
         * |     GB|            1|            1|                   6|           3973|
         * |     HU|            1|            1|                   3|           4389|
         * |     CA|            1|            1|                   2|           2380|
         * country                  -> country to which the IP address maps to
         * rebill_period            -> in months
         * payments_made            -> number of payments received from this customer
         * chapter_access_count     -> number of chapters customer watched at during the first half of their free trail
         * seconds_watched          -> seconds watched at during the first half of their free trail
         * */
        dataset = dataset
                .withColumn("country",
                        callUDF("countryGroupingUDF", col("country")))
                .withColumn("label",
                        when( col("payments_made").geq(1), lit(1)).otherwise(0)
                );

        StringIndexer countryIndexer = new StringIndexer();
        dataset = countryIndexer.setInputCol("country").setOutputCol("countryIndex")
                .fit(dataset).transform(dataset);

//        Dataset<Row> countryIndexes = dataset.select("countryIndex");
//        IndexToString indexToString = new  IndexToString();
//        indexToString.setInputCol("countryIndex").setOutputCol("countryIndexToString").transform(countryIndexes).show();
        new  IndexToString().setInputCol("countryIndex")
                            .setOutputCol("countryIndexToString")
                            .transform(dataset.select("countryIndex").distinct())
                            .show();
        VectorAssembler vectorAssembler = new VectorAssembler();
        vectorAssembler
            .setInputCols(new String[]{"countryIndex", "rebill_period", "chapter_access_count", "seconds_watched"})
            .setOutputCol("features");
        Dataset<Row> inputData = vectorAssembler.transform(dataset).select("label", "features");

        Dataset<Row>[] splitDatasets = inputData.randomSplit(new double[]{0.8, 0.2});
        Dataset<Row> trainDataset = splitDatasets[0];
        Dataset<Row> holdOutDataset = splitDatasets[1];

//        DecisionTreeRegressor decisionTreeRegressor = new DecisionTreeRegressor();
//        decisionTreeRegressor.setMaxDepth(3); // how many branches
//        DecisionTreeRegressionModel dTRModel = decisionTreeRegressor.fit(trainDataset);

        DecisionTreeClassifier decisionTreeClassifier = new DecisionTreeClassifier();
        decisionTreeClassifier.setMaxDepth(3); // how many branches
        DecisionTreeClassificationModel dTCModel = decisionTreeClassifier.fit(trainDataset);

        Dataset<Row> predictions = dTCModel.transform(holdOutDataset);
        predictions.show();

        logInfo(dTCModel.toDebugString()); // FeatureIndex -> see VectorAssembler
        /** DecisionTreeClassificationModel (uid=dtc_0056bc4237f5) of depth 3 with 15 nodes
         *   If (feature 3 <= 11170.0)
         *   *If (feature 3 <= 2.5)
         *   **If (feature 0 in {0.0,1.0,2.0,3.0,5.0})
         *   ***Predict: 0.0
         *   **Else (feature 0 not in {0.0,1.0,2.0,3.0,5.0})
         *   ***Predict: 0.0
         *   *Else (feature 3 > 2.5)
         *   **If (feature 2 <= 5.5)
         *   ***Predict: 1.0
         *   **Else (feature 2 > 5.5)
         *   ***Predict: 0.0
         *   Else (feature 3 > 11170.0)
         *   *If (feature 2 <= 10.5)
         *   **If (feature 3 <= 20752.5)
         *   ***Predict: 1.0
         *   **Else (feature 3 > 20752.5)
         *   ***Predict: 1.0
         *   *Else (feature 2 > 10.5)
         *   **If (feature 1 <= 3.5)
         *   ***Predict: 0.0
         *   **Else (feature 1 > 3.5)
         *   ***Predict: 1.0
         **/

        MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator();
        evaluator.setMetricName("accuracy");
        logInfo("ACCURACY OF MODEL = "+evaluator.evaluate(predictions));

        RandomForestClassifier randomForestClassifier = new RandomForestClassifier();
        randomForestClassifier.setMaxDepth(3);
        RandomForestClassificationModel randomForestClassificationModel = randomForestClassifier.fit(trainDataset);
        Dataset<Row> predictions2 = randomForestClassificationModel.transform(holdOutDataset);
        predictions2.show();
        logInfo(randomForestClassificationModel.toDebugString());
        logInfo("ACCURACY OF MODEL WITH RANDOM_FOREST_CLASSIFIER = " +evaluator.evaluate(predictions2));


//        countryIndexes.show();

//        dataset.show();





        sparkSession.close();
    }
}
